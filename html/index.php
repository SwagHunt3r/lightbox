<?php

?>

<head>
	<script src="/js/jquery-3.4.1.min.js"></script>
	<script src="/js/send-commands.js"></script>
</head>

<style>

	.light_hex {
		position: absolute;
		width: 60px;
		height: 60px;
		background-color: red;
	}
	
	.pixel {
		width: 10px;
		height: 10px;
		background-color: blue;
	}
	
	.light_hex.index_0 { left: 30px; top: 0px; }
	.light_hex.index_1 { left: 100px; top: 0px; }
	.light_hex.index_2 { left: 170px; top: 0px; }
	
	.light_hex.index_3 { left: 0px; top: 70px; }
	.light_hex.index_4 { left: 70px; top: 70px; }
	.light_hex.index_5 { left: 140px; top: 70px; }
	.light_hex.index_6 { left: 210px; top: 70px; }
	
	.light_hex.index_7 { left: 30px; top: 140px; }
	.light_hex.index_8 { left: 100px; top: 140px; }
	.light_hex.index_9 { left: 170px; top: 140px; }
	
	.light_hex.index_10 { left: 0px; top: 210px; }
	.light_hex.index_11 { left: 70px; top: 210px; }
	.light_hex.index_12 { left: 140px; top: 210px; }
	.light_hex.index_13 { left: 210px; top: 210px; }
	
	.light_hex.index_14 { left: 30px; top: 280px; }
	.light_hex.index_15 { left: 100px; top: 280px; }
	.light_hex.index_16 { left: 170px; top: 280px; }
	
	.light_hex.index_17 { left: 0px; top: 350px; }
	.light_hex.index_18 { left: 70px; top: 350px; }
	.light_hex.index_19 { left: 140px; top: 350px; }
	.light_hex.index_20 { left: 210px; top: 350px; }
	
	.light_hex.index_21 { left: 30px; top: 420px; }
	.light_hex.index_22 { left: 100px; top: 420px; }
	.light_hex.index_23 { left: 170px; top: 420px; }

</style>

<body>
	<button class="led_on">LED On</button>
	<button class="led_off">LED Off</button>
	
	<?php
	for ($i = 0; $i < 24; $i++)
	{ ?>
		<div class="light_hex index_<?= $i; ?>">
			<div class="pixel" style="position: absolute; left: 10px; top: 0px;"></div>
			<div class="pixel" style="position: absolute; left: 30px; top: 0px;"></div>
			<div class="pixel" style="position: absolute; left: 0px; top: 30px;"></div>
			<div class="pixel" style="position: absolute; left: 50px; top: 30px;"></div>
			<div class="pixel" style="position: absolute; left: 10px; top: 50px;"></div>
			<div class="pixel" style="position: absolute; left: 30px; top: 50px;"></div>
		</div>
	<?php
	} ?>
</body>