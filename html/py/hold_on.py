import neopixel
import time
import board
from digitalio import DigitalInOut, Direction
 
pixels = neopixel.NeoPixel(board.D18, 12)
 
pad_pin = board.D24
 
pad = DigitalInOut(pad_pin)
pad.direction = Direction.INPUT

while True:
    
    if pad.value is True: 
      pixels.fill((0, 0, 0))
    
    if pad.value is False:
      pixels.fill((255, 0, 0))
    
    time.sleep(0.1)