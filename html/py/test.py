import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 6)

nR = 31 #color value, R=red
nG = 31
nB = 31

nDir = [1, 0, 0]

while 1 :
        nR += nDir[0]
        nG += nDir[1]
        nB += nDir[2]
    
        pixels.fill((nR, nG, nB))
    
        if nR >= 255 :
            nDir[0] = -1
            nDir[1] = 1
        elif nR <= 30 and nDir[0] != 1 :
            nDir[0] = 0
        if nG >= 255 :
            nDir[1] = -1
            nDir[2] = 1
        elif nG <= 30 and nDir[1] != 1  :
            nDir[1] = 0
        if nB >= 255 :
            nDir[2] = -1
            nDir[0] = 1
        elif nB <= 30 and nDir[2] != 1  :
            nDir[2] = 0
        
        time.sleep(0.001)

