import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 288)

while True:

    pixels[0] = (255, 0, 0)  #r
    pixels[1] = (255, 165, 0)  #o 
    pixels[2] = (255, 255, 0)    #y
    pixels[3] = (0, 255, 0)   #g
    pixels[4] = (0, 0, 255)    #b
    pixels[5] = (153, 0, 230)   #v

    time.sleep(0.001)

    pixels[0] = (255, 165, 0)   #o
    pixels[1] = (255, 255, 0)   #y
    pixels[2] = (0, 255, 0)    #g
    pixels[3] = (0, 0, 255)    #b
    pixels[4] = (153, 0, 230)    #v
    pixels[5] = (255, 0, 0)    #r

    time.sleep(0.001)

    pixels[0] = (255, 255, 0)   #y
    pixels[1] = (0, 255, 0)    #g   
    pixels[2] = (0, 0, 255)    #b
    pixels[3] = (153, 0, 230)    #v
    pixels[4] = (255, 0, 0)    #r
    pixels[5] = (255, 165, 0)     #o

    time.sleep(0.001)

    pixels[0] = (0, 255, 0)   #g
    pixels[1] = (0, 0, 255)   #b
    pixels[2] = (153, 0, 230)   #v
    pixels[3] = (255, 0, 0)   #r
    pixels[4] = (255, 165, 0)    #o
    pixels[5] = (255, 255, 0)    #y

    time.sleep(0.001)
    
    pixels[0] = (0, 0, 255)    #b
    pixels[1] = (153, 0, 230)    #v
    pixels[2] = (255, 0, 0)    #r
    pixels[3] = (255, 165, 0)     #o
    pixels[4] = (255, 255, 0)     #y
    pixels[5] = (0, 255, 0)    #g
    
    time.sleep(0.001)

    pixels[0] = (153, 0, 230)    #v
    pixels[1] = (255, 0, 0)    #r
    pixels[2] = (255, 165, 0)    #o
    pixels[3] = (255, 255, 0)     #y
    pixels[4] = (0, 255, 0)     #g
    pixels[5] = (0, 0, 255)    #b

    time.sleep(0.001)

