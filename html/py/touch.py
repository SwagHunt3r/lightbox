import time
import board
from digitalio import DigitalInOut, Direction
 
pad_pin = board.D24
 
pad = DigitalInOut(pad_pin)
pad.direction = Direction.INPUT

while True:
 
    if pad.value:
        print(pad.value)
 
    time.sleep(0.1)