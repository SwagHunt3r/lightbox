import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 5)

pixels.fill((0, 255, 0))
    
count = 0     
        
while 1:
    pixels[0] = (0, 0, 0)
    pixels[1] = (0, 0, 0)
    pixels[2] = (0, 0, 0)
    
    pixels[count] = (255, 69, 0)
    
    count = count + 1 
    
    if count >= 6:
        count = 0
        
    time.sleep(0.1)
    