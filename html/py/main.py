import board
import neopixel
import time
import datetime
import numpy
from digitalio import DigitalInOut, Direction
from rpi_ws281x import Color, PixelStrip, ws

# LED strip configuration:
LED_COUNT = 288         # Number of LED pixels.
LED_PIN = 18           # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ = 800000   # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10           # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255   # Set to 0 for darkest and 255 for brightest
LED_INVERT = False     # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0
LED_STRIP = ws.SK6812_STRIP_RGBW
# LED_STRIP = ws.SK6812W_STRIP

pin = board.D18

pixels = neopixel.NeoPixel(pin, 288, auto_write=False)

aPixelHexes = []
 
pad_pin = board.D24
 
pad = DigitalInOut(pad_pin)
pad.direction = Direction.INPUT

def AddColors(color_1 = [], color_2 = []) :
    new_color = [0,0,0]
    new_color[0] = color_1[0] + color_2[0]
    new_color[1] = color_1[1] + color_2[1]
    new_color[2] = color_1[2] + color_2[2]
    
    if new_color[0] > 255 :
        new_color[0] = 255
    elif new_color[0] < 0 :
        new_color[0] = 0

    if new_color[1] > 255 :
        new_color[1] = 255
    elif new_color[1] < 0 :
        new_color[1] = 0

    if new_color[2] > 255 :
        new_color[2] = 255
    elif new_color[2] < 0 :
        new_color[2] = 0

    return new_color

def Color4Lerp(nPrevColor = [], nNextColor = [], nCurrentColor = [], fTime = 1) :
	nCurrentColor[0] = nPrevColor[0] + (nNextColor[0] - nPrevColor[0]) * fTime
	nCurrentColor[1] = nPrevColor[1] + (nNextColor[1] - nPrevColor[1]) * fTime
	nCurrentColor[2] = nPrevColor[2] + (nNextColor[2] - nPrevColor[2]) * fTime
	nCurrentColor[3] = nPrevColor[3] + (nNextColor[3] - nPrevColor[3]) * fTime
    
    #return nCurrentColor

class LightHex  :    
    # six lights, each with an x position, y position, and LED ID
    
    def __init__(self) :
        self.lights = numpy.zeros((6,3), dtype=int)
    
    def UpdateLights(self) :
        
        for x in range (6) :
            
            hor = int(self.lights[x][0])
            vert = int(self.lights[x][1])

            r = int(nPixels[hor][vert][0])
            g = int(nPixels[hor][vert][1])
            b = int(nPixels[hor][vert][2])

            pixels[int(self.lights[x][2])] = (r, g, b)
            pixels[int(self.lights[x][2]) + 1] = (r, g, b)
            # strip.setPixelColor(int(self.lights[x][2]), Color(r, g, b))
            # strip.setPixelColor(int(self.lights[x][2] + 1), Color(r, g, b))

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)


def RainbowCycle(strip, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256 * iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel(((i * 256 // strip.numPixels()) + j) & 255))
        strip.show()
        #time.sleep(wait_ms / 1000.0)

switch = 0
setting = 0

# Init PixelHexes
nSwap = 0
nSwapRow = 0
nSwapColumn = 0
nLightCount = 0

nCurLight = 0

for x in range (24) :
    instance = LightHex()
    aPixelHexes.append(instance)

for x in range (24) :
    if nSwap == 0 :
        aPixelHexes[x].lights[0][0] = 3 + (nSwapColumn * 4)
        aPixelHexes[x].lights[0][1] = 6 + (nSwapRow * 6)
        aPixelHexes[x].lights[0][2] = 0 + nCurLight
        
        aPixelHexes[x].lights[1][0] = 2 + (nSwapColumn * 4)
        aPixelHexes[x].lights[1][1] = 7 + (nSwapRow * 6)
        aPixelHexes[x].lights[1][2] = 2 + nCurLight

        aPixelHexes[x].lights[2][0] = 1 + (nSwapColumn * 4)
        aPixelHexes[x].lights[2][1] = 6 + (nSwapRow * 6)
        aPixelHexes[x].lights[2][2] = 4 + nCurLight

        aPixelHexes[x].lights[3][0] = 1 + (nSwapColumn * 4)
        aPixelHexes[x].lights[3][1] = 4 + (nSwapRow * 6)
        aPixelHexes[x].lights[3][2] = 6 + nCurLight
        
        aPixelHexes[x].lights[4][0] = 2 + (nSwapColumn * 4)
        aPixelHexes[x].lights[4][1] = 3 + (nSwapRow * 6)
        aPixelHexes[x].lights[4][2] = 8 + nCurLight
        
        aPixelHexes[x].lights[5][0] = 3 + (nSwapColumn * 4)
        aPixelHexes[x].lights[5][1] = 4 + (nSwapRow * 6)
        aPixelHexes[x].lights[5][2] = 10 + nCurLight

        nCurLight += 12
        nSwapRow += 1

        if nSwapRow == 3 :
            nSwapColumn += 1
            nSwapRow = 0
            nSwap = 1

    else :
        aPixelHexes[x].lights[5][0] = 1 + (nSwapColumn * 4)
        aPixelHexes[x].lights[5][1] = 1 + (18 - nSwapRow * 6)
        aPixelHexes[x].lights[5][2] = 0 + nCurLight
        
        aPixelHexes[x].lights[4][0] = 2 + (nSwapColumn * 4)
        aPixelHexes[x].lights[4][1] = 0 + (18 - nSwapRow * 6)
        aPixelHexes[x].lights[4][2] = 2 + nCurLight
        
        aPixelHexes[x].lights[3][0] = 3 + (nSwapColumn * 4)
        aPixelHexes[x].lights[3][1] = 1 + (18 - nSwapRow * 6)
        aPixelHexes[x].lights[3][2] = 4 + nCurLight

        aPixelHexes[x].lights[2][0] = 3 + (nSwapColumn * 4)
        aPixelHexes[x].lights[2][1] = 3 + (18 - nSwapRow * 6)
        aPixelHexes[x].lights[2][2] = 6 + nCurLight
        
        aPixelHexes[x].lights[1][0] = 2 + (nSwapColumn * 4)
        aPixelHexes[x].lights[1][1] = 4 + (18 - nSwapRow * 6)
        aPixelHexes[x].lights[1][2] = 8 + nCurLight
        
        aPixelHexes[x].lights[0][0] = 1 + (nSwapColumn * 4)
        aPixelHexes[x].lights[0][1] = 4 + (18 - nSwapRow * 6)
        aPixelHexes[x].lights[0][2] = 10 + nCurLight

        nCurLight += 12
        nSwapRow += 1

        if nSwapRow == 4 :
            nSwapColumn += 1
            nSwapRow = 0
            nSwap = 0

nPixels = numpy.zeros((32, 24, 3), dtype=int)

for x in range (31) :
    for y in range (24) :
        nPixels[x][y] = [0, 255, 0]

nCurColor = numpy.zeros ((32, 24, 3), dtype=int)

# Create NeoPixel object with appropriate configuration.
# strip = PixelStrip(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
# Intialize the library (must be called once before other functions).
# strip.begin()

last_time = datetime.datetime.now()
this_time = 0
while True :

    this_time = datetime.datetime.now()

    deltaTime = this_time - last_time
    print(deltaTime.microseconds)

    last_time = this_time

    
    if pad.value:        
        switch = 0
    
    else :
        if switch == 0 :
            if setting == 0:
                setting = 1
            else:
                setting = 0
            switch = 1
    
    if setting == 1 :
        nPixels[1][5] = [255, 0, 0]

    # Traverse and bleed color to adjacent squares

    SCALE = 20
    for x in range (32) :
        for y in range (24) :
            if switch == 1:
                r = nPixels[x][y][0] / SCALE
                g = nPixels[x][y][1] / SCALE
                b = nPixels[x][y][2] / SCALE
                small_color = [ r, g, b ]
                # if x > 0:
                #     nPixels[x-1][y] = AddColors(nPixels[x-1][y], small_color)
                #     if y > 0:
                #         nPixels[x-1][y-1] = AddColors(nPixels[x-1][y-1], small_color)
                #     if y < 23:
                #         nPixels[x-1][y+1] = AddColors(nPixels[x-1][y+1], small_color)
                # if x < 31:
                #     nPixels[x+1][y] = AddColors(nPixels[x+1][y], small_color)
                #     if y > 0:
                #         nPixels[x+1][y-1] = AddColors(nPixels[x+1][y-1], small_color)
                #     if y < 23:
                #         nPixels[x+1][y+1] = AddColors(nPixels[x+1][y+1], small_color)
                # if y > 0:
                #     nPixels[x][y-1] = AddColors(nPixels[x][y-1], small_color)
                # if y < 23:
                #     nPixels[x][y+1] = AddColors(nPixels[x][y+1], small_color)

            if nPixels[x][y][0] > 2 :
                nPixels[x][y][0] -= 3
            if nPixels[x][y][1] > 2 :
                nPixels[x][y][1] -= 3
            if nPixels[x][y][2] > 2 :
                nPixels[x][y][2] -= 3

    for i in range (24) :
        aPixelHexes[i].UpdateLights()

    # for i in range(24) :
    #     for j in range(6) :
    #         x = aPixelHexes[i].lights[j][0]
    #         y = aPixelHexes[i].lights[j][1]

    #         nPixels[x][y][0] = 255
    #         nPixels[x][y][1] = 0
    #         nPixels[x][y][2] = 0

    #         aPixelHexes[x].UpdateLights()

    #         pixels.show()

    #         time.sleep(0.1)

    pixels.show()
    # strip.show()
    
    time.sleep(0.01)


    

    
