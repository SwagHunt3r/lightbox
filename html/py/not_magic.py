import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 12)

pixels[0] = (0, 0, 255)
pixels[1] = (128, 0, 21)
pixels[2] = (0, 0, 255)
pixels[3] = (128, 0, 21)
pixels[4] = (0, 0, 255)
pixels[5] = (128, 0, 21)
pixels[6] = (0, 0, 255)
pixels[7] = (128, 0, 21)
pixels[8] = (0, 0, 255)
pixels[9] = (128, 0, 21)
pixels[10] = (0, 0, 255)
pixels[11] = (128, 0, 21)
