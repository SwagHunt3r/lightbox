import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 288)

pixel_config = [
    (51, 0, 51),
    (51, 0, 51),
    (128, 0, 21),
    (128, 0, 21),
    (51, 0, 51),
    (51, 0, 51),
    (128, 0, 21),
    (128, 0, 21),
    (51, 0, 51),
    (51, 0, 51),
    (128, 0, 21),
    (128, 0, 21)
]

runningIdx = 0
endingIdx = len(pixel_config)
for i in range(288):
    # Start new sequence if end is detected
    runningIdx = 0 if runningIdx == endingIdx else runningIdx

    pixels[i] = pixel_config[runningIdx]
    runningIdx += 1
