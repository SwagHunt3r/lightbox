import board
import neopixel
import time
import numpy
from digitalio import DigitalInOut, Direction


pixels = neopixel.NeoPixel(board.D18, 288)
 
pad_pin = board.D24
 
pad = DigitalInOut(pad_pin)
pad.direction = Direction.INPUT


switch = 0;
setting = 0;

while True :

    
    if pad.value:
        print(pad.value);
        switch = 0;
    
    else :
        print(pad.value);
        if switch == 0 :
            if setting == 0:
                setting = 1;
            
            else:
                pixels.fill((0, 0, 0))
        
    if setting == 0 :
    
        pixels.fill((255, 85, 0));
    
    
    time.sleep(0.001)

