import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 288)

pixel_config = [
    (230, 153, 0),
    (230, 153, 0),
    (230,153, 0),
    (230, 153, 0),
    (230, 230, 0),
    (230, 230, 0),
    (153, 230, 0),
    (153, 230, 0),
    (153, 230, 0),
    (153, 230, 0),
    (230, 230, 0),
    (230, 230, 0)
]

runningIdx = 0
endingIdx = len(pixel_config)
for i in range(288):
    # Start new sequence if end is detected
    runningIdx = 0 if runningIdx == endingIdx else runningIdx

    pixels[i] = pixel_config[runningIdx]
    runningIdx += 1
