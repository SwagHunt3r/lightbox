import board
import neopixel
import time
import numpy
from digitalio import DigitalInOut, Direction


pixels = neopixel.NeoPixel(board.D18, 288)
 
pad_pin = board.D24
 
pad = DigitalInOut(pad_pin)
pad.direction = Direction.INPUT


switch = 0;
setting = 0;

while True :

    
    if pad.value:
        print(pad.value);
        switch = 0;
    
    else :
        print(pad.value);
        if switch == 0 :
            if setting == 0:
                setting = 1;
            else:
                setting = 0;
            switch = 1
        
    if setting == 1 :
        pixels.fill((0,0,0));    
    else :
        while True:

            pixels[0] = (255, 0, 0)  #r
            pixels[1] = (255, 0, 0)  #r
            pixels[2] = (255, 165, 0)  #o
            pixels[3] = (255, 165, 0)  #o
            pixels[4] = (255, 255, 0)    #y
            pixels[5] = (255, 255, 0)    #y
            pixels[6] = (0, 255, 0)   #g
            pixels[7] = (0, 255, 0)    #g
            pixels[8] = (0, 0, 255)    #b
            pixels[9] = (0, 0, 255)    #b
            pixels[10] = (153, 0, 230)   #v
            pixels[11] = (153, 0, 230)   #v

            time.sleep(0.09)

            pixels[0] = (255, 165, 0)   #o
            pixels[1] = (255, 165, 0)  #o
            pixels[2] = (255, 255, 0)   #y
            pixels[3] = (255, 255, 0)    #y
            pixels[4] = (0, 255, 0)    #g
            pixels[5] = (0, 255, 0)    #g
            pixels[6] = (0, 0, 255)    #b
            pixels[7] = (0, 0, 255)    #b
            pixels[8] = (153, 0, 230)    #v
            pixels[9] = (153, 0, 230)   #v
            pixels[10] = (255, 0, 0)    #r
            pixels[11] = (255, 0, 0)    #r
    
            time.sleep(0.09)
    
            pixels[0] = (255, 255, 0)   #y
            pixels[1] = (255, 255, 0)    #y
            pixels[2] = (0, 255, 0)    #g
            pixels[3] = (0, 255, 0)    #g
            pixels[4] = (0, 0, 255)    #b
            pixels[5] = (0, 0, 255)    #b
            pixels[6] = (153, 0, 230)    #v
            pixels[7] = (153, 0, 230)   #v
            pixels[8] = (255, 0, 0)    #r
            pixels[9] = (255, 0, 0)    #r
            pixels[10] = (255, 165, 0)     #o
            pixels[11] = (255, 165, 0)  #o

            time.sleep(0.09)
    
            pixels[0] = (0, 255, 0)   #g
            pixels[1] = (0, 255, 0)    #g
            pixels[2] = (0, 0, 255)   #b
            pixels[3] = (0, 0, 255)    #b
            pixels[4] = (153, 0, 230)   #v
            pixels[5] = (153, 0, 230)   #v
            pixels[6] = (255, 0, 0)   #r
            pixels[7] = (255, 0, 0)    #r
            pixels[8] = (255, 165, 0)    #o
            pixels[9] = (255, 165, 0)  #o
            pixels[10] = (255, 255, 0)    #y
            pixels[11] = (255, 255, 0)    #y

            time.sleep(0.09)
    
            pixels[0] = (0, 0, 255)    #b
            pixels[1] = (0, 0, 255)    #b
            pixels[2] = (153, 0, 230)    #v
            pixels[3] = (153, 0, 230)   #v
            pixels[4] = (255, 0, 0)    #r
            pixels[5] = (255, 0, 0)    #r
            pixels[6] = (255, 165, 0)     #o
            pixels[7] = (255, 165, 0)  #o
            pixels[8] = (255, 255, 0)     #y
            pixels[9] = (255, 255, 0)    #y
            pixels[10] = (0, 255, 0)    #g
            pixels[11] = (0, 255, 0)    #g
    
            time.sleep(0.09)

            pixels[0] = (153, 0, 230)    #v
            pixels[1] = (153, 0, 230)   #v
            pixels[2] = (255, 0, 0)    #r
            pixels[3] = (255, 0, 0)    #r
            pixels[4] = (255, 165, 0)    #o
            pixels[5] = (255, 165, 0)  #o
            pixels[6] = (255, 255, 0)     #y
            pixels[7] = (255, 255, 0)    #y
            pixels[8] = (0, 255, 0)    #g
            pixels[9] = (0, 255, 0)    #g
            pixels[10] = (0, 0, 255)    #b
            pixels[11] = (0, 0, 255)    #b

            time.sleep(0.09);
    
    
    time.sleep(0.001)

