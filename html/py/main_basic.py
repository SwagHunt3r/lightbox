import board
import neopixel
import time
import datetime
import numpy
from digitalio import DigitalInOut, Direction
from rpi_ws281x import Color, PixelStrip, ws

# LED strip configuration:
LED_COUNT = 288         # Number of LED pixels.
LED_PIN = 18           # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ = 800000   # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10           # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255   # Set to 0 for darkest and 255 for brightest
LED_INVERT = False     # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0
LED_STRIP = ws.SK6812_STRIP_RGBW
# LED_STRIP = ws.SK6812W_STRIP

pin = board.D18

pixels = neopixel.NeoPixel(pin, 288, auto_write=False)

aPixelHexes = []
 
pad_pin = [ board.D3, board.D4, board.D17, board.D27, board.D22, board.D10, board.D9, board.D11, board.D5, board.D6, board.D13, board.D19, board.D26, board.D14, board.D15, board.D23, board.D24, board.D25, board.D8, board.D7, board.D12, board.D16, board.D20, board.D21 ]

pad = []

switch = numpy.zeros((24), dtype=int)
switch_set = numpy.zeros((24), dtype=int)
setting = numpy.zeros((24), dtype=int)

for i in range(24) :
    pad.append(DigitalInOut(pad_pin[i]))
    pad[i].direction = Direction.INPUT

def AddColors(color_1 = [], color_2 = []) :
    new_color = [0,0,0]
    new_color[0] = color_1[0] + color_2[0]
    new_color[1] = color_1[1] + color_2[1]
    new_color[2] = color_1[2] + color_2[2]
    
    if new_color[0] > 255 :
        new_color[0] = 255
    elif new_color[0] < 0 :
        new_color[0] = 0

    if new_color[1] > 255 :
        new_color[1] = 255
    elif new_color[1] < 0 :
        new_color[1] = 0

    if new_color[2] > 255 :
        new_color[2] = 255
    elif new_color[2] < 0 :
        new_color[2] = 0

    return new_color

nColorLerp = [0, 0, 0]

def Color3Lerp(nPrevColor = [], nNextColor = [], fTime = 1) :
	nColorLerp[0] = nPrevColor[0] + (nNextColor[0] - nPrevColor[0]) * fTime
	nColorLerp[1] = nPrevColor[1] + (nNextColor[1] - nPrevColor[1]) * fTime
	nColorLerp[2] = nPrevColor[2] + (nNextColor[2] - nPrevColor[2]) * fTime

class LightHex  :    
    # six lights, each with an x position, y position, and LED ID
    
    def __init__(self) :
        self.lights = numpy.zeros((6, 4), dtype=int)
        self.new_color = [0, 0, 0]
        self.curColor = -1
        self.colorCycle = 0
        self.cycleCounter = 0.0
        self.cycleSpot = 0
        self.lastColor = [0, 0, 0]
    
    def NextColor(self) :

        self.curColor = self.curColor + 1

        if self.curColor == 14 :
            self.curColor = 0
            self.colorCycle = 0

        if self.curColor == 0 :# red
            self.new_color = [255, 0, 0]
        elif self.curColor == 1 :# orange
            self.new_color = [255, 127, 0]
        elif self.curColor == 2 :# yellow
            self.new_color = [255, 255, 0]
        elif self.curColor == 3 :# green-yellow
            self.new_color = [127, 255, 0]
        elif self.curColor == 4 : # red
            self.new_color = [0, 255, 0]
        elif self.curColor == 5 : # bluish green
            self.new_color = [0, 255, 127]
        elif self.curColor == 6 :# teal
            self.new_color = [0, 255, 255]
        elif self.curColor == 7 :# greenish blue
            self.new_color = [0, 127, 255]
        elif self.curColor == 8 :# teal
            self.new_color = [0, 0, 255]
        elif self.curColor == 9 :# purple
            self.new_color = [127, 0, 255]
        elif self.curColor == 10 :# lavender
            self.new_color = [255, 0, 255]
        elif self.curColor == 11 : # pink
            self.new_color = [255, 0, 127]
        elif self.curColor == 12 : # off
            self.new_color = [0, 0, 0]
        elif self.curColor == 13 : # Rainbow 1
            self.new_color = [0, 0, 255]
            self.lastColor = [0, 127, 255]
            self.colorCycle = 1
    
    def ColorCycleUpdate(self) :
        self.cycleSpot = self.cycleSpot + 1
        
        self.lastColor[0] = self.new_color[0]
        self.lastColor[1] = self.new_color[1]
        self.lastColor[2] = self.new_color[2]

        if self.cycleSpot == 12 :
            self.cycleSpot = 0

        if self.cycleSpot == 0 :# red
            self.new_color = [255, 0, 0]
        elif self.cycleSpot == 1 :# orange
            self.new_color = [255, 127, 0]
        elif self.cycleSpot == 2 :# yellow
            self.new_color = [255, 255, 0]
        elif self.cycleSpot == 3 :# green-yellow
            self.new_color = [127, 255, 0]
        elif self.cycleSpot == 4 : # red
            self.new_color = [0, 255, 0]
        elif self.cycleSpot == 5 : # bluish green
            self.new_color = [0, 255, 127]
        elif self.cycleSpot == 6 :# teal
            self.new_color = [0, 255, 255]
        elif self.cycleSpot == 7 :# greenish blue
            self.new_color = [0, 127, 255]
        elif self.cycleSpot == 8 :# teal
            self.new_color = [0, 0, 255]
        elif self.cycleSpot == 9 :# purple
            self.new_color = [127, 0, 255]
        elif self.cycleSpot == 10 :# lavender
            self.new_color = [255, 0, 255]
        elif self.cycleSpot == 11 : # pink
            self.new_color = [255, 0, 127]

    def UpdateLights(self) :
        for i in range (6) :
            
            # Rainbow 1
            if self.colorCycle == 1 :
                if self.cycleCounter < 1 :
                    self.cycleCounter += 0.01
                    
                    Color3Lerp(self.lastColor, self.new_color, self.cycleCounter)

                    self.lights[i][0] = nColorLerp[0]
                    self.lights[i][1] = nColorLerp[1]
                    self.lights[i][2] = nColorLerp[2]
                
                else :
                    self.ColorCycleUpdate()
                    self.cycleCounter = 0

            else :
                self.lights[i][0] = self.new_color[0]
                self.lights[i][1] = self.new_color[1]
                self.lights[i][2] = self.new_color[2]

            r = self.lights[i][0]
            g = self.lights[i][1]
            b = self.lights[i][2]

            pixels[self.lights[i][3]] = (r, g, b)
            pixels[self.lights[i][3] + 1] = (r, g, b)

# Init PixelHexes
nSwap = 0
nSwapRow = 0
nSwapColumn = 0
nLightCount = 0

nCurLight = 0

for x in range (24) :
    instance = LightHex()
    aPixelHexes.append(instance)

for x in range (24) :
    if nSwap == 0 :
        aPixelHexes[x].lights[0][3] = 0 + nCurLight
        aPixelHexes[x].lights[1][3] = 2 + nCurLight
        aPixelHexes[x].lights[2][3] = 4 + nCurLight
        aPixelHexes[x].lights[3][3] = 6 + nCurLight
        aPixelHexes[x].lights[4][3] = 8 + nCurLight
        aPixelHexes[x].lights[5][3] = 10 + nCurLight

        nCurLight += 12

    else :
        aPixelHexes[x].lights[5][3] = 0 + nCurLight
        aPixelHexes[x].lights[4][3] = 2 + nCurLight
        aPixelHexes[x].lights[3][3] = 4 + nCurLight
        aPixelHexes[x].lights[2][3] = 6 + nCurLight
        aPixelHexes[x].lights[1][3] = 8 + nCurLight
        aPixelHexes[x].lights[0][3] = 10 + nCurLight

        nCurLight += 12

for i in range(24) :
    aPixelHexes[i].NextColor()

pixels.show()

while True :
    
    for i in range(24) :

        if pad[i].value:        
            switch[i] = 0
        
        else :
            if switch[i] == 0 :
                if setting[i] == 0:
                    setting[i] = 1
                else:
                    setting[i] = 0
                switch[i] = 1
                switch_set[i] = 1
        
        if switch_set[i] == 1 :
            print('hit')
            aPixelHexes[i].NextColor()
            switch_set[i] = 0
                
        aPixelHexes[i].UpdateLights()
    
    pixels.show()
    
    time.sleep(0.001)


    

    
