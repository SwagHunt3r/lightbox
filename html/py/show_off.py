import board
import neopixel
import time



pixels = neopixel.NeoPixel(board.D18, 12)


pixels.fill((255, 0, 0))

time.sleep(2)  

pixels.fill((255, 85, 0))

time.sleep(2)

pixels.fill((255, 213, 0))

time.sleep(2)

pixels.fill((0, 255, 0))

time.sleep(2)

pixels.fill((0, 0, 255))

time.sleep(2)

pixels.fill((153, 0, 230))

time.sleep(2)

pixels.fill((51, 0, 51))

time.sleep(2)

pixels.fill((0, 230, 230))

time.sleep(2)

pixels.fill((128, 0, 21))

time.sleep(2)

pixels.fill((34, 102, 0))

time.sleep(2)

pixels.fill((0, 153, 128))

time.sleep(2)

pixels.fill((0, 0, 77))

time.sleep(2)

pixels.fill((230, 0, 115))

time.sleep(2)

pixels.fill((255, 255, 0))

time.sleep(2)

pixels.fill((255, 255, 255))

time.sleep(2)

pixels[0] = (25, 255, 255)
pixels[1] = (25, 255, 255)
pixels[2] = (25, 255, 255)
pixels[3] = (25, 255, 255)
pixels[4] = (155, 155, 155)
pixels[5] = (155, 155, 155)
pixels[6] = (0, 0, 255)
pixels[7] = (0, 0, 255)
pixels[8] = (0, 0, 255)
pixels[9] = (0, 0, 255)
pixels[10] = (155, 155, 155)
pixels[11] = (155, 155, 155)

time.sleep(2)

pixels[0] = (0, 0, 255)
pixels[1] = (0, 0, 255)
pixels[2] = (0, 0, 255)
pixels[3] = (0, 0, 255)
pixels[4] = (0, 255, 0)
pixels[5] = (0, 255, 0)
pixels[6] = (0, 153, 128)
pixels[7] = (0, 153, 128)
pixels[8] = (0, 153, 128)
pixels[9] = (0, 153, 128)
pixels[10] = (0, 255, 0)
pixels[11] = (0, 255, 0)

time.sleep(2)

pixels[0] = (0, 0, 255)
pixels[1] = (0, 0, 255)
pixels[2] = (0, 0, 255)
pixels[3] = (0, 0, 255)
pixels[4] = (34, 204, 0)
pixels[5] = (34, 204, 0)
pixels[6] = (255, 255, 0)
pixels[7] = (255, 255, 0)
pixels[8] = (255, 255, 0)
pixels[9] = (255, 255, 0)
pixels[10] = (34, 204, 0)
pixels[11] = (34, 204, 0)

time.sleep(2)

pixels[0] = (0, 0, 255)
pixels[1] = (255, 213, 0)
pixels[2] = (0, 0, 255)
pixels[3] = (255, 213, 0)
pixels[4] = (0, 0, 255)
pixels[5] = (255, 213, 0)
pixels[6] = (0, 0, 255)
pixels[7] = (255, 213, 0)
pixels[8] = (0, 0, 255)
pixels[9] = (255, 213, 0)
pixels[10] = (0, 0, 255)
pixels[11] = (255, 213, 0)

time.sleep(2)

pixels[0] = (255, 213, 0)
pixels[1] = (255, 213, 0)
pixels[2] = (255, 213, 0)
pixels[3] = (255, 213, 0)
pixels[4] = (0, 255, 213)
pixels[5] = (0, 255, 213)
pixels[6] = (170, 0, 255)
pixels[7] = (170, 0, 255)
pixels[8] = (170, 0, 255)
pixels[9] = (170, 0, 255)
pixels[10] = (0, 255, 213)
pixels[11] = (0, 255, 213)

time.sleep(2)

pixels[0] = (128, 0, 21)
pixels[1] = (255, 85, 0)
pixels[2] = (128, 0, 21)
pixels[3] = (255, 85, 0)
pixels[4] = (128, 0, 21)
pixels[5] = (255, 85, 0)
pixels[6] = (128, 0, 21)
pixels[7] = (255, 85, 0)
pixels[8] = (128, 0, 21)
pixels[9] = (255, 85, 0)
pixels[10] = (128, 0, 21)
pixels[11] = (255, 85, 0)

time.sleep(2)

pixels[0] = (255, 128, 0)
pixels[1] = (255, 128, 0)
pixels[2] = (170, 0, 255)
pixels[3] = (170, 0, 255)
pixels[4] = (255, 128, 0)
pixels[5] = (255, 128, 0)
pixels[6] = (170, 0, 255)
pixels[7] = (170, 0, 255)
pixels[8] = (255, 128, 0)
pixels[9] = (255, 128, 0)
pixels[10] = (170, 0, 255)
pixels[11] = (170, 0, 255)

time.sleep(2)

pixels[0] = (153, 0, 153)
pixels[1] = (153, 0, 153)
pixels[2] = (153, 0, 153)
pixels[3] = (153, 0, 153)
pixels[4] = (77, 0, 153)
pixels[5] = (77, 0, 153)
pixels[6] = (153, 0, 127)
pixels[7] = (153, 0, 127)
pixels[8] = (153, 0, 127)
pixels[9] = (153, 0, 127)
pixels[10] = (77, 0, 153)
pixels[11] = (77, 0, 153)

time.sleep(2)

pixels[0] = (255, 0, 0)
pixels[1] = (255, 0, 0)
pixels[2] = (255, 0, 0)
pixels[3] = (255, 0, 0)
pixels[4] = (255, 128, 0)
pixels[5] = (255, 128, 0)
pixels[6] = (255, 255, 0)
pixels[7] = (255, 255, 0)
pixels[8] = (255, 255, 0)
pixels[9] = (255, 255, 0)
pixels[10] = (255, 128, 0)
pixels[11] = (255, 128, 0)

time.sleep(2)

pixels[0] = (255, 0, 0)
pixels[1] = (255, 0, 0)
pixels[2] = (255, 0, 0)
pixels[3] = (255, 0, 0)
pixels[4] = (153, 0,230)
pixels[5] = (153, 0, 230)
pixels[0] = (0, 0, 255)
pixels[1] = (0, 0, 255)
pixels[2] = (0, 0, 255)
pixels[3] = (0, 0, 255)
pixels[4] = (153, 0, 230)
pixels[5] = (153, 0, 230)

time.sleep(2)

pixels[0] = (255, 0, 0)
pixels[1] = (255, 0, 0)
pixels[2] = (255, 165, 0)
pixels[3] = (255, 165, 0)
pixels[4] = (255, 255, 0)
pixels[5] = (255, 255, 0)
pixels[6] = (0, 255, 0)
pixels[7] = (0, 255, 0)
pixels[8] = (0, 0, 255)
pixels[9] = (0, 0, 255)
pixels[10] = (153, 0, 230)
pixels[11] = (153, 0, 230)

time.sleep(2)

pixels[0] = (51, 0, 51)
pixels[1] = (51, 0, 51)
pixels[2] = (51, 0, 51)
pixels[3] = (128, 0, 21)
pixels[4] = (128, 0, 21)
pixels[5] = (128, 0, 21)
pixels[6] = (51, 0, 51)
pixels[7] = (51, 0, 51)
pixels[8] = (51, 0, 51)
pixels[9] = (128, 0, 21)
pixels[10] = (128, 0, 21)
pixels[11] = (128, 0, 21)

time.sleep(2)

pixels[0] = (51, 0, 51)
pixels[1] = (51, 0, 51)
pixels[2] = (230, 0, 51)
pixels[3] = (230, 0, 21)
pixels[4] = (51, 0, 21)
pixels[5] = (51, 0, 21)
pixels[6] = (230, 0, 51)
pixels[7] = (230, 0, 51)
pixels[8] = (51, 0, 51)
pixels[9] = (51, 0, 21)
pixels[10] = (230, 0, 21)
pixels[11] = (230, 0, 21)

time.sleep(2)

pixels[0] = (51, 0, 51)
pixels[1] = (51, 0, 51)
pixels[2] = (128, 0, 21)
pixels[3] = (128, 0, 21)
pixels[4] = (51, 0, 51)
pixels[5] = (51, 0, 51)
pixels[6] = (128, 0, 21)
pixels[7] = (128, 0, 21)
pixels[8] = (51, 0, 51)
pixels[9] = (51, 0, 51)
pixels[10] = (128, 0, 21)
pixels[11] = (128, 0, 21)

time.sleep(2)

pixels[0] = (230, 153, 0)
pixels[1] = (230, 153, 0)
pixels[2] = (230, 153, 0)
pixels[3] = (230, 153, 0)
pixels[4] = (230, 230, 0)
pixels[5] = (230, 230, 0)
pixels[6] = (153, 230, 0)
pixels[7] = (153, 230, 0)
pixels[8] = (153, 230, 0)
pixels[9] = (153, 230, 0)
pixels[10] = (230, 230, 0)
pixels[11] = (230, 230, 0)

time.sleep(2)

while True:

    pixels[0] = (255, 0, 0)  #r
    pixels[1] = (255, 0, 0)  #r
    pixels[2] = (255, 165, 0)  #o
    pixels[3] = (255, 165, 0)  #o
    pixels[4] = (255, 255, 0)    #y
    pixels[5] = (255, 255, 0)    #y
    pixels[6] = (0, 255, 0)   #g
    pixels[7] = (0, 255, 0)    #g
    pixels[8] = (0, 0, 255)    #b
    pixels[9] = (0, 0, 255)    #b
    pixels[10] = (153, 0, 230)   #v
    pixels[11] = (153, 0, 230)   #v

    time.sleep(0.09)

    pixels[0] = (255, 165, 0)   #o
    pixels[1] = (255, 165, 0)  #o
    pixels[2] = (255, 255, 0)   #y
    pixels[3] = (255, 255, 0)    #y
    pixels[4] = (0, 255, 0)    #g
    pixels[5] = (0, 255, 0)    #g
    pixels[6] = (0, 0, 255)    #b
    pixels[7] = (0, 0, 255)    #b
    pixels[8] = (153, 0, 230)    #v
    pixels[9] = (153, 0, 230)   #v
    pixels[10] = (255, 0, 0)    #r
    pixels[11] = (255, 0, 0)    #r

    time.sleep(0.09)

    pixels[0] = (255, 255, 0)   #y
    pixels[1] = (255, 255, 0)    #y
    pixels[2] = (0, 255, 0)    #g
    pixels[3] = (0, 255, 0)    #g
    pixels[4] = (0, 0, 255)    #b
    pixels[5] = (0, 0, 255)    #b
    pixels[6] = (153, 0, 230)    #v
    pixels[7] = (153, 0, 230)   #v
    pixels[8] = (255, 0, 0)    #r
    pixels[9] = (255, 0, 0)    #r
    pixels[10] = (255, 165, 0)     #o
    pixels[11] = (255, 165, 0)  #o

    time.sleep(0.09)

    pixels[0] = (0, 255, 0)   #g
    pixels[1] = (0, 255, 0)    #g
    pixels[2] = (0, 0, 255)   #b
    pixels[3] = (0, 0, 255)    #b
    pixels[4] = (153, 0, 230)   #v
    pixels[5] = (153, 0, 230)   #v
    pixels[6] = (255, 0, 0)   #r
    pixels[7] = (255, 0, 0)    #r
    pixels[8] = (255, 165, 0)    #o
    pixels[9] = (255, 165, 0)  #o
    pixels[10] = (255, 255, 0)    #y
    pixels[11] = (255, 255, 0)    #y

    time.sleep(0.09)
    
    pixels[0] = (0, 0, 255)    #b
    pixels[1] = (0, 0, 255)    #b
    pixels[2] = (153, 0, 230)    #v
    pixels[3] = (153, 0, 230)   #v
    pixels[4] = (255, 0, 0)    #r
    pixels[5] = (255, 0, 0)    #r
    pixels[6] = (255, 165, 0)     #o
    pixels[7] = (255, 165, 0)  #o
    pixels[8] = (255, 255, 0)     #y
    pixels[9] = (255, 255, 0)    #y
    pixels[10] = (0, 255, 0)    #g
    pixels[11] = (0, 255, 0)    #g
    
    time.sleep(0.09)

    pixels[0] = (153, 0, 230)    #v
    pixels[1] = (153, 0, 230)   #v
    pixels[2] = (255, 0, 0)    #r
    pixels[3] = (255, 0, 0)    #r
    pixels[4] = (255, 165, 0)    #o
    pixels[5] = (255, 165, 0)  #o
    pixels[6] = (255, 255, 0)     #y
    pixels[7] = (255, 255, 0)    #y
    pixels[8] = (0, 255, 0)    #g
    pixels[9] = (0, 255, 0)    #g
    pixels[10] = (0, 0, 255)    #b
    pixels[11] = (0, 0, 255)    #b

    time.sleep(0.09)


