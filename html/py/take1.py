from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def plot_gradient_series(color_dict, filename,
             pointsize=100, control_points=None):
  ''' Take a dictionary containing the color
      gradient in RBG and hex form and plot
      it to a 3D matplotlib device '''

  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  xcol = color_dict["r"]
  ycol = color_dict["g"]
  zcol = color_dict["b"]

  # We can pass a vector of colors
  # corresponding to each point
  ax.scatter(xcol, ycol, zcol,
             c=color_dict["hex"], s=pointsize)

  # If bezier control points passed to function,
  # plot along with curve
  if control_points != None:
    xcntl = control_points["r"]
    ycntl = control_points["g"]
    zcntl = control_points["b"]
    ax.scatter( xcntl, ycntl, zcntl,
                c=control_points["hex"],
                s=pointsize, marker='s')

  ax.set_xlabel('Red Value')
  ax.set_ylabel('Green Value')
  ax.set_zlabel('Blue Value')
  ax.set_zlim3d(0,255)
  plt.ylim(0,255)
  plt.xlim(0,255)

  # Save two views of each plot
  ax.view_init(elev=15, azim=68)
  plt.savefig(filename + ".svg")
  ax.view_init(elev=15, azim=28)
  plt.savefig(filename + "_view_2.svg")

  # Show plot for testing
  plt.show()
