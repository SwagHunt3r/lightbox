import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 24)

while True:

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)   

    time.sleep(0.09)

    pixels[0] = (255, 0, 0)   #r
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)   
    pixels[3] = (0, 0, 0)    
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)    
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)   
    pixels[10] = (0, 0, 0)    
    pixels[11] = (0, 0, 0)    
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)   
    pixels[13] = (0, 0, 0)   
    pixels[14] = (0, 0, 0)   
    pixels[15] = (0, 0, 0) 
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)    
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)   
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    

    time.sleep(0.09)

    pixels[0] = (255, 255, 0)   #r
    pixels[1] = (255, 255, 0)    #r
    pixels[2] = (0, 0, 0)    
    pixels[3] = (0, 0, 0)    
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)    
    pixels[7] = (0, 0, 0)   
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)     
    pixels[11] = (0, 0, 0)  
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)   
    pixels[13] = (0, 0, 0)    
    pixels[14] = (0, 0, 0)    
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)    
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)     
    pixels[23] = (0, 0, 0)  


    time.sleep(0.09)

    pixels[0] = (255, 85, 0)   #o
    pixels[1] = (255, 0, 0)    #r
    pixels[2] = (255, 0, 0)   #r
    pixels[3] = (0, 0, 0)    
    pixels[4] = (0, 0, 0)   
    pixels[5] = (0, 0, 0)   
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)  
    pixels[10] = (0, 0, 0)    
    pixels[11] = (0, 0, 0)    
   
    time.sleep(0.09)
   
    pixels[12] = (0, 0, 0)   
    pixels[13] = (0, 0, 0)    
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)   
    pixels[17] = (0, 0, 0)   
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)  
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    

    time.sleep(0.09)
    
    pixels[0] = (255, 85, 0)    #o
    pixels[1] = (255, 85, 0)    #o
    pixels[2] = (255, 0, 0)    #r
    pixels[3] = (255, 0, 0)   #r
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)     
    pixels[7] = (0, 0, 0)  
    pixels[8] = (0, 0, 0)     
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)    
    pixels[11] = (0, 0, 0)    
   
    time.sleep(0.09)
   
    pixels[12] = (0, 0, 0)    
    pixels[13] = (0, 0, 0)    
    pixels[14] = (0, 0, 0)    
    pixels[15] = (0, 0, 0)   
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)     
    pixels[19] = (0, 0, 0)  
    pixels[20] = (0, 0, 0)     
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    
    
    time.sleep(0.09)

    pixels[0] = (255, 255, 0)   #y
    pixels[1] = (255, 85, 0)  #o
    pixels[2] = (255, 85, 0)    #o
    pixels[3] = (255, 0, 0)    #r
    pixels[4] = (255, 0, 0)   #r 
    pixels[5] = (0, 0, 0)  
    pixels[6] = (0, 0, 0)     
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)    
    pixels[11] = (0, 0, 0)    
   
    time.sleep(0.09)
   
    pixels[12] = (0, 0, 0)    
    pixels[13] = (0, 0, 0)   
    pixels[14] = (0, 0, 0)    
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)  
    pixels[18] = (0, 0, 0)     
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    

    time.sleep(0.09)

    pixels[0] = (255, 255, 0)  #y
    pixels[1] = (255, 255, 0)  #y
    pixels[2] = (255, 85, 0)  #o
    pixels[3] = (255, 85, 0)  #o
    pixels[4] = (255, 255, 0)    #r
    pixels[5] = (255, 255, 0)    #r
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)   

    time.sleep(0.09)

    pixels[0] = (0, 255, 0)   #g
    pixels[1] = (255, 255, 0)  #y
    pixels[2] = (255, 255, 0)   #y
    pixels[3] = (255, 85, 0)    #o
    pixels[4] = (255, 85, 0)    #o
    pixels[5] = (255, 0, 0)    #r
    pixels[6] = (255, 0, 0)    #r
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)   
    pixels[10] = (0, 0, 0)    
    pixels[11] = (0, 0, 0)    
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)   
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)   
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)    
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)   
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    

    time.sleep(0.09)

    pixels[0] = (0, 255, 0)   #g
    pixels[1] = (0, 255, 0)    #g
    pixels[2] = (255, 255, 0)    #y
    pixels[3] = (255, 255, 0)    #y
    pixels[4] = (255, 85, 0)    #o
    pixels[5] = (255, 85, 0)    #o
    pixels[6] = (255, 0, 0)    #r
    pixels[7] = (255, 0, 0)   #r
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)     
    pixels[11] = (0, 0, 0)  
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)   
    pixels[13] = (0, 0, 0)    
    pixels[14] = (0, 0, 0)    
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)    
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)     
    pixels[23] = (0, 0, 0)  


    time.sleep(0.09)

    pixels[0] = (0, 0, 255)   #b
    pixels[1] = (0, 255, 0)    #g
    pixels[2] = (0, 255, 0)   #g
    pixels[3] = (255, 255, 0)    #y
    pixels[4] = (255, 255, 0)   #y
    pixels[5] = (255, 85, 0)   #o
    pixels[6] = (255, 85, 0)   #o
    pixels[7] = (255, 0, 0)    #r
    pixels[8] = (255, 0, 0)    #r
    pixels[9] = (0, 0, 0)  
    pixels[10] = (0, 0, 0)    
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)   
    pixels[13] = (0, 0, 0)    
    pixels[14] = (0, 0, 0)   
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)   
    pixels[17] = (0, 0, 0)   
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)  
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    

    time.sleep(0.09)
    
    pixels[0] = (0, 0, 255)    #b
    pixels[1] = (0, 0, 255)    #b
    pixels[2] = (0, 255, 0)    #g
    pixels[3] = (0, 255, 0)   #g
    pixels[4] = (255, 255, 0)    #y
    pixels[5] = (255, 255, 0)    #y
    pixels[6] = (255, 85, 0)     #o
    pixels[7] = (255, 85, 0)  #o
    pixels[8] = (255, 0, 0)    #r
    pixels[9] = (255, 0, 0)    #r
    pixels[10] = (0, 0, 0)    
    pixels[11] = (0, 0, 0)    
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)    
    pixels[13] = (0, 0, 0)    
    pixels[14] = (0, 0, 0)   
    pixels[15] = (0, 0, 0)   
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)     
    pixels[19] = (0, 0, 0)  
    pixels[20] = (0, 0, 0)     
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)
    pixels[23] = (0, 0, 0)    
    
    time.sleep(0.09)

    pixels[0] = (153, 0, 230)    #v
    pixels[1] = (0, 0, 230)   #b
    pixels[2] = (0, 0, 230)    #b
    pixels[3] = (0, 255, 0)    #g
    pixels[4] = (0, 255, 0)    #g
    pixels[5] = (255, 255, 0)  #y
    pixels[6] = (255, 255, 0)     #y
    pixels[7] = (255, 85, 0)    #o
    pixels[8] = (255, 85, 0)    #o
    pixels[9] = (255, 0, 0)    #r
    pixels[10] = (255, 0, 0)     #r
    pixels[11] = (0, 0, 0)    
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)    
    pixels[13] = (0, 0, 0)   
    pixels[14] = (0, 0, 0)    
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)  
    pixels[18] = (0, 0, 0)     
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    

    time.sleep(0.09)

    pixels[0] = (153, 0, 230)  #v
    pixels[1] = (153, 0, 230)  #v
    pixels[2] = (0, 0, 230)  #b
    pixels[3] = (0, 0, 230)  #b
    pixels[4] = (0, 255, 0)    #g
    pixels[5] = (0, 255, 0)    #g
    pixels[6] = (255, 255, 0)   #y
    pixels[7] = (255, 255, 0)    #y
    pixels[8] = (255, 85, 0)    #o
    pixels[9] = (255, 85, 0)    #o
    pixels[10] = (255, 0, 0)   #r
    pixels[11] = (255, 0, 0)   #r
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)   

    time.sleep(0.09)

    pixels[0] = (0, 0, 0)   
    pixels[1] = (153, 0, 230)  #v
    pixels[2] = (153, 0, 230)   #v
    pixels[3] = (0, 0, 230)    #b
    pixels[4] = (0, 0, 230)    #b
    pixels[5] = (0, 255, 0)    #g
    pixels[6] = (0, 255, 0)    #g
    pixels[7] = (255, 255, 0)    #y
    pixels[8] = (153, 255, 0)    #y
    pixels[9] = (255, 85, 0)   #o
    pixels[10] = (255, 85, 0)    #o
    pixels[11] = (255, 0, 0)    #r
    
    time.sleep(0.09)
    
    pixels[12] = (255, 0, 0)  #r
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)   
    pixels[15] = (0, 0, 0)    
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)    
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)   
    pixels[22] = (0, 0, 0)    
    pixels[23] = (0, 0, 0)    

    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (153, 0, 230)  #v
    pixels[3] = (153, 0, 230)   #v
    pixels[4] = (0, 0, 230)    #b
    pixels[5] = (0, 0, 230)    #b
    pixels[6] = (0, 255, 0)    #g
    pixels[7] = (0, 255, 0)    #g
    pixels[8] = (255, 255, 0)    #y
    pixels[9] = (153, 255, 0)    #y
    pixels[10] = (255, 85, 0)   #o
    pixels[11] = (255, 85, 0)    #o
    
    time.sleep(0.09)
    
    pixels[12] = (255, 0, 0)    #r
    pixels[13] = (255, 0, 0)  #r
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)   
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (153, 0, 230)  #v
    pixels[4] = (153, 0, 230)   #v
    pixels[5] = (0, 0, 230)    #b
    pixels[6] = (0, 0, 230)    #b
    pixels[7] = (0, 255, 0)    #g
    pixels[8] = (0, 255, 0)    #g
    pixels[9] = (255, 255, 0)    #y
    pixels[10] = (153, 255, 0)    #y
    pixels[11] = (255, 85, 0)   #o
    
    time.sleep(0.09)
    
    pixels[12] = (255, 85, 0)    #o
    pixels[13] = (255, 0, 0)    #r
    pixels[14] = (255, 0, 0)  #r
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)
    pixels[4] = (153, 0, 230)  #v
    pixels[5] = (153, 0, 230)   #v
    pixels[6] = (0, 0, 230)    #b
    pixels[7] = (0, 0, 230)    #b
    pixels[8] = (0, 255, 0)    #g
    pixels[9] = (0, 255, 0)    #g
    pixels[10] = (255, 255, 0)    #y
    pixels[11] = (153, 255, 0)    #y
    
    time.sleep(0.09)
    
    pixels[12] = (255, 85, 0)   #o
    pixels[13] = (255, 85, 0)    #o
    pixels[14] = (255, 0, 0)    #r
    pixels[15] = (255, 0, 0)  #r
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)   
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)    
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)   
    pixels[22] = (0, 0, 0)
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (153, 0, 230)  #v
    pixels[6] = (153, 0, 230)   #v
    pixels[7] = (0, 0, 230)    #b
    pixels[8] = (0, 0, 230)    #b
    pixels[9] = (0, 255, 0)    #g
    pixels[10] = (0, 255, 0)    #g
    pixels[11] = (255, 255, 0)    #y
    
    time.sleep(0.09)
    
    pixels[12] = (153, 255, 0)    #y
    pixels[13] = (255, 85, 0)   #o
    pixels[14] = (255, 85, 0)    #o
    pixels[15] = (255, 0, 0)    #r
    pixels[16] = (255, 0, 0)  #r
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (153, 0, 230)  #v
    pixels[7] = (153, 0, 230)   #v
    pixels[8] = (0, 0, 230)    #b
    pixels[9] = (0, 0, 230)    #b
    pixels[10] = (0, 255, 0)    #g
    pixels[11] = (0, 255, 0)    #g
    
    time.sleep(0.09)
    
    pixels[12] = (255, 255, 0)    #y
    pixels[13] = (153, 255, 0)    #y
    pixels[14] = (255, 85, 0)   #o
    pixels[15] = (255, 85, 0)    #o
    pixels[16] = (255, 0, 0)    #r
    pixels[17] = (255, 0, 0)  #r
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (153, 0, 230)  #v
    pixels[8] = (153, 0, 230)   #v
    pixels[9] = (0, 0, 230)    #b
    pixels[10] = (0, 0, 230)    #b
    pixels[11] = (0, 255, 0)    #g
    
    time.sleep(0.09)
    
    pixels[12] = (0, 255, 0)    #g
    pixels[13] = (255, 255, 0)    #y
    pixels[14] = (153, 255, 0)    #y
    pixels[15] = (255, 85, 0)   #o
    pixels[16] = (255, 85, 0)    #o
    pixels[17] = (255, 0, 0)    #r
    pixels[18] = (255, 0, 0)  #r
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)
    pixels[8] = (153, 0, 230)  #v
    pixels[9] = (153, 0, 230)   #v
    pixels[10] = (0, 0, 230)    #b
    pixels[11] = (0, 0, 230)    #b
    
    time.sleep(0.09)
    
    pixels[12] = (0, 255, 0)    #g
    pixels[13] = (0, 255, 0)    #g
    pixels[14] = (255, 255, 0)    #y
    pixels[15] = (153, 255, 0)    #y
    pixels[16] = (255, 85, 0)   #o
    pixels[17] = (255, 85, 0)    #o
    pixels[18] = (255, 0, 0)    #r
    pixels[19] = (255, 0, 0)  #r
    pixels[20] = (0, 0, 0)   
    pixels[21] = (0, 0, 0)
    pixels[22] = (0, 0, 0)
    pixels[23] = (0, 0, 0)

    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (153, 0, 230)  #v
    pixels[10] = (153, 0, 230)   #v
    pixels[11] = (0, 0, 230)    #b
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 230)    #b
    pixels[13] = (0, 255, 0)    #g
    pixels[14] = (0, 255, 0)    #g
    pixels[15] = (255, 255, 0)    #y
    pixels[16] = (153, 255, 0)    #y
    pixels[17] = (255, 85, 0)   #o
    pixels[18] = (255, 85, 0)    #o
    pixels[19] = (255, 0, 0)    #r
    pixels[20] = (255, 0, 0)  #r
    pixels[21] = (0, 0, 0)   
    pixels[22] = (0, 0, 0)
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (153, 0, 230)  #v
    pixels[11] = (153, 0, 230)   #v
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 230)    #b
    pixels[13] = (0, 0, 230)    #b
    pixels[14] = (0, 255, 0)    #g
    pixels[15] = (0, 255, 0)    #g
    pixels[16] = (255, 255, 0)    #y
    pixels[17] = (153, 255, 0)    #y
    pixels[18] = (255, 85, 0)   #o
    pixels[19] = (255, 85, 0)    #o
    pixels[20] = (255, 0, 0)    #r
    pixels[21] = (255, 0, 0)  #r
    pixels[22] = (0, 0, 0)
    pixels[23] = (0, 0, 0)

    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (153, 0, 230)  #v
    
    time.sleep(0.09)
    
    pixels[12] = (153, 0, 230)   #v
    pixels[13] = (0, 0, 230)    #b
    pixels[14] = (0, 0, 230)    #b
    pixels[15] = (0, 255, 0)    #g
    pixels[16] = (0, 255, 0)    #g
    pixels[17] = (255, 255, 0)    #y
    pixels[18] = (153, 255, 0)    #y
    pixels[19] = (255, 85, 0)   #o
    pixels[20] = (255, 85, 0)    #o
    pixels[21] = (255, 0, 0)    #r
    pixels[22] = (255, 0, 0)  #r
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 0)  
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (153, 0, 230)  #v
    pixels[13] = (153, 0, 230)   #v
    pixels[14] = (0, 0, 230)    #b
    pixels[15] = (0, 0, 230)    #b
    pixels[16] = (0, 255, 0)    #g
    pixels[17] = (0, 255, 0)    #g
    pixels[18] = (255, 255, 0)    #y
    pixels[19] = (153, 255, 0)    #y
    pixels[20] = (255, 85, 0)   #o
    pixels[21] = (255, 85, 0)    #o
    pixels[22] = (255, 0, 0)    #r
    pixels[23] = (255, 0, 0)  #r
    
    time.sleep(0.09)

    pixels[0] = (255, 0, 0)  #r
    pixels[1] = (0, 0, 0)  
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (153, 0, 230)  #v
    pixels[14] = (153, 0, 230)   #v
    pixels[15] = (0, 0, 230)    #b
    pixels[16] = (0, 0, 230)    #b
    pixels[17] = (0, 255, 0)    #g
    pixels[18] = (0, 255, 0)    #g
    pixels[19] = (255, 255, 0)    #y
    pixels[20] = (153, 255, 0)    #y
    pixels[21] = (255, 85, 0)   #o
    pixels[22] = (255, 85, 0)    #o
    pixels[23] = (255, 0, 0)    #r
    
    time.sleep(0.09)

    pixels[0] = (255, 0, 0)  #r
    pixels[1] = (255, 0, 0)  #r
    pixels[2] = (0, 0, 0)  
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (153, 0, 230)  #v
    pixels[15] = (153, 0, 230)   #v
    pixels[16] = (0, 0, 230)    #b
    pixels[17] = (0, 0, 230)    #b
    pixels[18] = (0, 255, 0)    #g
    pixels[19] = (0, 255, 0)    #g
    pixels[20] = (255, 255, 0)    #y
    pixels[21] = (153, 255, 0)    #y
    pixels[22] = (255, 85, 0)   #o
    pixels[23] = (255, 85, 0)    #o
    
    time.sleep(0.09)

    pixels[0] = (255, 85, 0)  #o
    pixels[1] = (255, 0, 0)  #r
    pixels[2] = (255, 0, 0)  #r
    pixels[3] = (0, 0, 0)  
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (153, 0, 230)  #v
    pixels[16] = (153, 0, 230)   #v
    pixels[17] = (0, 0, 230)    #b
    pixels[18] = (0, 0, 230)    #b
    pixels[19] = (0, 255, 0)    #g
    pixels[20] = (0, 255, 0)    #g
    pixels[21] = (255, 255, 0)    #y
    pixels[22] = (153, 255, 0)    #y
    pixels[23] = (255, 85, 0)   #o
  
    
    time.sleep(0.09)

    pixels[0] = (255, 85, 0)  #o
    pixels[1] = (255, 85, 0)  #o
    pixels[2] = (255, 0, 0)  #r
    pixels[3] = (255, 0, 0)  #r
    pixels[4] = (0, 0, 0)    
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (153, 0, 230)  #v
    pixels[17] = (153, 0, 230)   #v
    pixels[18] = (0, 0, 230)    #b
    pixels[19] = (0, 0, 230)    #b
    pixels[20] = (0, 255, 0)    #g
    pixels[21] = (0, 255, 0)    #g
    pixels[22] = (255, 255, 0)    #y
    pixels[23] = (153, 255, 0)    #y

    
    time.sleep(0.09)

    pixels[0] = (255, 255, 0)  #y
    pixels[1] = (255, 85, 0)  #o
    pixels[2] = (255, 85, 0)  #o
    pixels[3] = (255, 0, 0)  #r
    pixels[4] = (255, 0, 0)  #r 
    pixels[5] = (0, 0, 0)    
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (153, 0, 230)  #v
    pixels[18] = (153, 0, 230)   #v
    pixels[19] = (0, 0, 230)    #b
    pixels[20] = (0, 0, 230)    #b
    pixels[21] = (0, 255, 0)    #g
    pixels[22] = (0, 255, 0)    #g
    pixels[23] = (255, 255, 0)    #y
  
    
    time.sleep(0.09)

    pixels[0] = (255, 255, 0)  #y
    pixels[1] = (255, 255, 0)  #y
    pixels[2] = (255, 85, 0)  #o
    pixels[3] = (255, 85, 0)  #o
    pixels[4] = (255, 0, 0)  #r  
    pixels[5] = (255, 0, 0)  #r 
    pixels[6] = (0, 0, 0)   
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (153, 0, 230)  #v
    pixels[19] = (153, 0, 230)   #v
    pixels[20] = (0, 0, 230)    #b
    pixels[21] = (0, 0, 230)    #b
    pixels[22] = (0, 255, 0)    #g
    pixels[23] = (0, 255, 0)    #g
    
    time.sleep(0.09)

    pixels[0] = (0, 255, 0)  #g
    pixels[1] = (255, 255, 0)  #y
    pixels[2] = (255, 255, 0)  #y
    pixels[3] = (255, 85, 0)  #o
    pixels[4] = (255, 85, 0)  #o  
    pixels[5] = (255, 0, 0)  #r  
    pixels[6] = (255, 0, 0)  #r 
    pixels[7] = (0, 0, 0)    
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (153, 0, 230)  #v
    pixels[20] = (153, 0, 230)   #v
    pixels[21] = (0, 0, 230)    #b
    pixels[22] = (0, 0, 230)    #b
    pixels[23] = (0, 255, 0)    #g

    
    time.sleep(0.09)

    pixels[0] = (0, 255, 0)  #g
    pixels[1] = (0, 255, 0)  #g
    pixels[2] = (255, 255, 0)  #y
    pixels[3] = (255, 255, 0)  #y
    pixels[4] = (255, 85, 0)  #o  
    pixels[5] = (255, 85, 0)  #o  
    pixels[6] = (255, 0, 0)  #r 
    pixels[7] = (255, 0, 0)  #r  
    pixels[8] = (0, 0, 0)    
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (153, 0, 230)  #v
    pixels[21] = (153, 0, 230)   #v
    pixels[22] = (0, 0, 230)    #b
    pixels[23] = (0, 0, 230)    #b
   
    
    time.sleep(0.09)

    pixels[0] = (0, 0, 230)  #b
    pixels[1] = (0, 255, 0)  #g
    pixels[2] = (0, 255, 0)  #g
    pixels[3] = (255, 255, 0)  #y
    pixels[4] = (255, 255, 0)  #y  
    pixels[5] = (255, 85, 0)  #o  
    pixels[6] = (255, 85, 0)  #o 
    pixels[7] = (255, 0, 0)  #r 
    pixels[8] = (255, 0, 0)  #r  
    pixels[9] = (0, 0, 0)    
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (153, 0, 230)  #v
    pixels[22] = (153, 0, 230)   #v
    pixels[23] = (0, 0, 230)    #b

    time.sleep(0.09)

    pixels[0] = (0, 0, 230)  #b
    pixels[1] = (0, 0, 230)  #b
    pixels[2] = (0, 255, 0)  #g
    pixels[3] = (0, 255, 0)  #g
    pixels[4] = (255, 255, 0)  #y  
    pixels[5] = (255, 255, 0)  #y  
    pixels[6] = (255,85, 0)  #o 
    pixels[7] = (255, 85, 0)  #o  
    pixels[8] = (255, 0, 0)  #r  
    pixels[9] = (255, 0, 0)  #r  
    pixels[10] = (0, 0, 0)   
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (153, 0, 230)  #v
    pixels[23] = (153, 0, 230)   #v
   
    time.sleep(0.09)

    pixels[0] = (153, 0, 230)  #v
    pixels[1] = (0, 0, 230)  #b
    pixels[2] = (0, 0, 230)  #b
    pixels[3] = (0, 255, 0)  #g
    pixels[4] = (0, 255, 0)  #g  
    pixels[5] = (255, 255, 0)  #y 
    pixels[6] = (255, 255, 0)  #y 
    pixels[7] = (255, 85, 0)  #o  
    pixels[8] = (255, 85, 0)  #o  
    pixels[9] = (255, 0, 0)  #r  
    pixels[10] = (255, 0, 0) #r  
    pixels[11] = (0, 0, 0)   
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (153, 0, 230)  #v
    
    
    
    time.sleep(0.09)

    pixels[0] = (153, 0, 230)  #v
    pixels[1] = (153, 0, 230)  #v
    pixels[2] = (0, 0, 230)  #b
    pixels[3] = (0, 0, 230)  #b
    pixels[4] = (0, 255, 0)  #g  
    pixels[5] = (0, 255, 0)  #g  
    pixels[6] = (255, 255, 0)  #y 
    pixels[7] = (255, 255, 0)  #y  
    pixels[8] = (255, 85, 0)  #o  
    pixels[9] = (255, 85, 0)  #o  
    pixels[10] = (255, 0, 0) #r  
    pixels[11] = (255, 0, 0) #r  
    
    time.sleep(0.09)
    
    pixels[12] = (0, 0, 0)  
    pixels[13] = (0, 0, 0)  
    pixels[14] = (0, 0, 0)  
    pixels[15] = (0, 0, 0)  
    pixels[16] = (0, 0, 0)    
    pixels[17] = (0, 0, 0)    
    pixels[18] = (0, 0, 0)   
    pixels[19] = (0, 0, 0)   
    pixels[20] = (0, 0, 0)    
    pixels[21] = (0, 0, 0)    
    pixels[22] = (0, 0, 0)   
    pixels[23] = (0, 0, 0)
    
    time.sleep(0.09)