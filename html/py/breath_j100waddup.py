import board
import neopixel
import time
pixels = neopixel.NeoPixel(board.D18, 6)

nR = [0, 0, 0, 0, 0, 0] #color value, R=red
nG = [0, 0, 0, 0, 0, 0]
nB = [0, 0, 0, 0, 0, 0]

r = 0
g = 120
b = 255

for i in range (0, 6) :
    nR[i] = r + i * 40
    #nG[i] = (g + i * 40) % 255
    #nB[i] = (b + i * 40) % 255

n = 6
m = 3
nDir = [[0] * m for i in range(n)]

for i in range (0, 6) :
    nDir [i][0] = 1
    nDir [i][1] = 0
    nDir [i][2] = 0

while 1 :
    for i in range (0, 6) : 
        nR[i] += nDir[i][0]
        nG[i] += nDir[i][1]
        nB[i] += nDir[i][2]
    
        pixels[i] = (nR[i], nG[i], nB[i])
    
        if nR[i] >= 255 :
            nDir[i][0] = -1
            nDir[i][1] = 1
        elif nR[i] <= 30 :
            nDir[i][0] = 0
        if nG[i] >= 255 :
            nDir[i][1] = -1
            nDir[i][2] = 1
        elif nG[i] <= 30 :
            nDir[i][1] = 1
        if nB[i] >= 255 :
            nDir[i][2] = -1
            nDir[i][0] = 1
        elif nB[i] <= 30 :
            nDir[i][2] = 1
        
        time.sleep(0.001)


    

    
